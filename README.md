# Tasks Extras

This module provides permissions-based access for users to manage the tasks of
others, and assign them. The module also contains some formtting enhancements,
for example to show a checkbox instead of a text link for marking tasks as done.
That module is optimized for the Olivero theme, but may be compatible with other
themes too.


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires a number of modules, aligned with the Tasks recipe:
Add Content by Bundle, Display Link Plus, DraggableViews, Storage, and Flag


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

 * After installing this module, you will have a new "Tasks Manage" view, with a
   block and a page display.


## Maintainers

 * Martin Anderson-Clutz - [mandclu](https://www.drupal.org/u/mandclu)
